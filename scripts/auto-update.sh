#!/bin/sh
# This script is run by a server once a week to keep the User-Agent list up-to-date automatically
set -eu #-o pipefail
cd "$(dirname "$(readlink -f "${0}")")/.."

# Update GIT repository (translations!)
git pull --ff-only

# Re-generate User-Agent list
scripts/update.sh

# Publish changes (if any)
exit_code=0
scripts/release.sh -apS || exit_code=$?
if [ ${exit_code} -ne 0 ] && [ ${exit_code} -ne 21 ];  # Exit code 21 means “No changes”
then
	exit ${exit_code}
fi
